# -*- coding: utf-8 -*-

import sys, json, contextlib, httplib, ssl, urllib2, socket, urllib, cookielib


 # acessa a url da pagina da ulbra para pegar o link de busca
from api.models import DetalhaLivro
from api.json_utils import obj2json

from BeautifulSoup import BeautifulSoup

class BuscaDetalhada(object):

  def __init__(self,numpag,numLivro, numBiblio):
    self.numpag = numpag
    self.numLivro = numLivro
    self.numBiblio = numBiblio

  def detalhada(self, get_hash = None):

    base_url = "https://memphis.ulbranet.com.br/ALEPH"

    num = int(self.numpag)
    numlivro = int(self.numLivro)
    numbiblio = int(self.numBiblio)

    request = urllib2.Request(base_url)
    response = urllib2.urlopen(request)

    the_page = response.read()
    pool = BeautifulSoup(the_page)

    results = pool.findAll('form', attrs={'name' : 'form1'})
    for result in results:
      linkBusca = result.get('action')
    #print(linkBusca)

    # realiza a busca no site
    url = linkBusca
    values = {'func' : 'find-b',
              'request' : 'teste',
              'find_code' : 'WRD',
              'adjacent' : 'N',
              'local_base' : 'ULB01',
              'x' : '40',
              'y' : '8', 
              'filter_code_4' : 'WMA',
              'filter_request_4': '',
              'filter_code_1' : 'WLN',
              'filter_request_1': '',
              'filter_code_2' : 'WYR',
              'filter_request_2': '',
              'filter_code_3' : 'WyR',
              'filter_request_3': ''}

    data = urllib.urlencode(values)
    req = urllib2.Request(url, data)
    response2 = urllib2.urlopen(req)
    page = response2.read()
    pool2 = BeautifulSoup(page)
    #results3 = pool2.findAll('tr', {'valign' : 'baseline'})
    #print (results3)
    #results3 = pool2.findAll('td', attrs={'class' : 'td1'})
    #num = 3
    if num > 1:
      #print 'numpag = ' +str(num)
      a = 1
      while a <= num :
        for next in pool2.findAll('a',{'title':'Next'}):
          nextPag = next.get('href')

          #buscando a proxima pagina
        request5 = urllib2.Request(nextPag)
        response5 = urllib2.urlopen(request5)

        the_page5 = response5.read()
        pool2 = BeautifulSoup(the_page5)
        
        a += 1
        #print 'a = '+str(a)
        if a == num :
          break

    detLivro = []
    i = 1
    for result in pool2.findAll('tr', {'valign' : 'baseline'}):#results3:
      j = 1
      for tmp in result.findAll("td"):
        #print tmp
        if j == 7 and i == numlivro:
          for det in tmp.findAll("a"):
            #print det.get('href')
            detLivro.append(det.get('href'))
        j += 1
      i += 1
    #print detLivro[numbiblio-1]

    request6 = urllib2.Request(detLivro[numbiblio-1])
    response6 = urllib2.urlopen(request6)
    the_page6 = response6.read()
    pool6 = BeautifulSoup(the_page6)

    print pool6
     #for result6 in pool6.find('tr'):#results3:
         #print result6
      #for tmp2 in result2.findAll("td"):
        #print tmp2

    #! imprime o array de dados
    # #print obj2json(livros)
    #if get_hash:
    #   try:
      #pool6.hash = hashlib.sha1(obj2json(pool6)).hexdigest()
    #   except Exception, e:
    #     print e.message
    #return pool6

    #print (results3)replace(" \n", "")