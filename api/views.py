# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from busca import Busca
from buscaDetalhada import BuscaDetalhada
from logaCgu import LogaCgu
from historico import Historico
from emprestimo import Emprestimos
from renovar import Renovar
from buscaDetalhada import BuscaDetalhada

from json_utils import obj2json

@csrf_exempt
def main(request):
	#tipo = "1"
	
	callback = request.GET['callback']
	tipo = request.GET['tipo']
	#cgu = request.POST['cgu']
	#adjacent = request.POST['adjacent']
	#find_code = request.POST['find_code']
	#local_base = request.POST['local_base']
	#palavra = request.POST['palavra']
	#numpag = request.POST['numpag']

	#if tipo == "1":
		#etalha = BuscaDetalhada("1", "3", "3")
		#data = detalha.detalhada()
		#historico = Historico("61259510")
		#data = historico.historico()
		#buscar = Busca("Y","WRD","ULB01","teste","1")
		#data = buscar.busca()

	# Chama a função buscar 
	if tipo == "buscar":
		adjacent = request.GET['adjacent']
		find_code = request.GET['find_code']
		local_base = request.GET['local_base']
		palavra = request.GET['palavra']
		numpag = request.GET['numpag']

		buscar = Busca(adjacent, find_code,local_base,palavra,numpag)
		data = buscar.busca()

	# Chama a função para logar no site da ulbra
	elif tipo == "logar":
		cgu = request.GET['cgu']

		loga = LogaCgu(cgu)
		data = loga.loga()

	# Chama a função para buscar o historico do usuario
	elif tipo == "historico":
		cgu = request.GET['cgu']

		historico = Historico(cgu)
		data = historico.historico()

	# Chama a função mostrar os emprestimos do usuario
	elif tipo == "emprestimo":
		cgu = request.GET['cgu']

		emprestimo = Emprestimos(cgu)
		data = emprestimo.emprestimo()

	# Chama a função para renovar os livros
	elif tipo == "renovar":
		cgu = request.GET['cgu']

		renovar = Renovar(cgu)
		data = renovar.renovar()

	if callback:
		xml_bytes = '%s(%s)' % (callback, obj2json(data))
	else :
		xml_bytes = obj2json(data)

	return HttpResponse(xml_bytes, content_type='application/javascript; charset=utf-8')
