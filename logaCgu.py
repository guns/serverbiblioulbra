# -*- coding: utf-8 -*-

import sys, json, contextlib, httplib, ssl, urllib2, socket, urllib, cookielib


 # acessa a url da pagina da ulbra para pegar o link de busca
from api.models import Loga
from api.json_utils import obj2json

from BeautifulSoup import BeautifulSoup

class LogaCgu(object):

	def __init__(self,cgu):
		self.cgu = cgu

	def loga(self, get_hash = None):

		cgu = self.cgu

		base_url = "https://memphis.ulbranet.com.br/ALEPH"

		request = urllib2.Request(base_url)
		response = urllib2.urlopen(request)

		the_page = response.read()
		pool = BeautifulSoup(the_page)

		results = pool.findAll('td', attrs={'class' : 'middlebar'})
		href = [] #! cria o array de dados
		for result in results:
		    for tmp in result.findAll("a", "blue"):
		        href.append(tmp.get('href'))
		#! imprime o array de dados
		#print(href[1])

		#! comeca a pegar o action do form para logar
		base_url2 = href[1]

		request2 = urllib2.Request(base_url2)
		response2 = urllib2.urlopen(request2)

		the_page2 = response2.read()
		pool2 = BeautifulSoup(the_page2)

		#!print(pool2)
		results2 = pool2.findAll('form', attrs={'name' : 'form1'})
		#href = [] #! cria o array de dados

		for result2 in results2:
		  action = result2.get('action')
		    #for tmp in result.findAll("a", "blue"):
		        #href.append(tmp.get('href'))
		#print(action)

		url = action
		values = {'func' : 'login-session',
		          'login_source' : 'LOGIN-BOR',
		          'bor_verification' : '123',
		          'login_source' : 'LOGIN-BOR',
		          'bor_library' : 'ULB50',
		          'bor_id' : cgu,
		          'x' : '82', 
		          'y' : '7' }

		data = urllib.urlencode(values)
		req = urllib2.Request(url, data)
		response2 = urllib2.urlopen(req)
		page = response2.read()
		#print(page)

		pool3 = BeautifulSoup(page)

		#!print(pool2)
		#results3 = pool3.findAll('tr')
		logar = Loga()
		i = 1
		for results3 in pool3.findAll('tr'):
			#print results3
			
			for tmp in results3.findAll("td", {"class":"td1"}):
				
				for tmp3 in tmp.findAll("a"):
					#print ' i = %s - %s' %(i, tmp3.string)
					if i == 2:
						logar.emprestimos = tmp3.string
					if i == 4:
						logar.historico = tmp3.string
					if i == 6:
						logar.caixa = tmp3.string
					if i == 8:
						logar.reservas = tmp3.string
					i += 1
		#! imprime o array de dados
		#print obj2json(logar)
		if get_hash:
		#    	try:
		     	logar.hash = hashlib.sha1(obj2json(logar)).hexdigest()
		#     except Exception, e:
		#        	print e.message
		return logar
