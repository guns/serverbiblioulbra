# -*- coding: utf-8 -*-

from busca import Busca
from buscaDetalhada import BuscaDetalhada
from logaCgu import LogaCgu
from historico import Historico
from emprestimo import Emprestimos
from renovar import Renovar

from api.json_utils import obj2json

import optparse

def main():
	p = optparse.OptionParser()
	p.add_option('--tipo', '-t')	
	#p.add_option('--password', '-p')
	options, arguments = p.parse_args()
	if options.tipo == "buscar":
		busca = Busca("Y","WRD","ULB01","teste","2")
		try:
			data = busca.busca()
			print obj2json(data)
		except Exception, e:
			print 'Could not fetch grades: %s' % e.message
	
	if options.tipo == "detalhe":
		detalha = BuscaDetalhada("1", "3", "3")
		try:
			data = detalha.detalhada()
			print obj2json(data)
		except Exception, e:
			print 'Could not fetch grades: %s' % e.message
	
	if options.tipo == "logar":
		loga = LogaCgu("61259510")
		try:
			data = loga.loga()
			print obj2json(data)
		except Exception, e:
			print 'Could not fetch grades: %s' % e.message

	if options.tipo == "historico":
		historico = Historico("61259510")
		try:
			data = historico.historico()
			print obj2json(data)
		except Exception, e:
			print 'Could not fetch grades: %s' % e.message

	if options.tipo == "emprestimo":
		emprestimo = Emprestimos("28133510")
		try:
			data = emprestimo.emprestimo()
			print obj2json(data)
		except Exception, e:
			print 'Could not fetch grades: %s' % e.message

	if options.tipo == "renovar":
		renovar = Renovar("61259510")
		try:
			data = renovar.renovar()
			print obj2json(data)
		except Exception, e:
			print 'Could not fetch grades: %s' % e.message

	#else:
		#print 'You must provide the username and password from Auto Atendimento.'


if __name__ == '__main__':
	main()
