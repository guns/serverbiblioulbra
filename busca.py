# -*- coding: utf-8 -*-

import sys, json, contextlib, httplib, ssl, urllib2, socket, urllib, cookielib


 # acessa a url da pagina da ulbra para pegar o link de busca
from api.models import Livro, Biblioteca, Pagina
from api.json_utils import obj2json

from BeautifulSoup import BeautifulSoup

class Busca(object):

  def __init__(self,adjacent,find_code,local_base,palavra,numpag):
    self.adjacent = adjacent
    self.find_code = find_code
    self.local_base = local_base
    self.palavra = palavra
    self.numpag = numpag

  def busca(self, get_hash = None):

    adjacent = self.adjacent
    find_code = self.find_code
    local_base = self.local_base
    palavra = self.palavra
    num = int(self.numpag)

    base_url = "https://memphis.ulbranet.com.br/ALEPH"

    request = urllib2.Request(base_url)
    response = urllib2.urlopen(request)

    the_page = response.read()
    pool = BeautifulSoup(the_page)

    results = pool.findAll('form', attrs={'name' : 'form1'})
    for result in results:
      linkBusca = result.get('action')
    #print(linkBusca)

    # realiza a busca no site
    url = linkBusca
    values = {'func' : 'find-b',
              'request' : palavra,
              'find_code' : find_code,
              'adjacent' : adjacent,
              'local_base' : local_base,
              'x' : '40',
              'y' : '8', 
              'filter_code_4' : 'WMA',
              'filter_request_4': '',
              'filter_code_1' : 'WLN',
              'filter_request_1': '',
              'filter_code_2' : 'WYR',
              'filter_request_2': '',
              'filter_code_3' : 'WyR',
              'filter_request_3': ''}

    data = urllib.urlencode(values)
    req = urllib2.Request(url, data)
    response2 = urllib2.urlopen(req)
    page = response2.read()
    pool2 = BeautifulSoup(page)
    #results3 = pool2.findAll('tr', {'valign' : 'baseline'})
    #print (results3)
    #results3 = pool2.findAll('td', attrs={'class' : 'td1'})
    #num = 3
    if num > 1:
      #print 'numpag = ' +str(num)
      a = 1
      while a <= num :
        for next in pool2.findAll('a',{'title':'Next'}):
          nextPag = next.get('href')

          #buscando a proxima pagina
        request5 = urllib2.Request(nextPag)
        response5 = urllib2.urlopen(request5)

        the_page5 = response5.read()
        pool2 = BeautifulSoup(the_page5)
        
        a += 1
        #print 'a = '+str(a)
        if a == num :
          break


    livro = []
    #livro = Livro()
    num = []
    for inform in pool2.findAll('td',{'id':'bold'}):
      num.append(inform.string.replace("     ", ""))
      #livro.numPag = inform.string
    numPag = num[0].replace("\n", "")
    i = 0
    #paginas = []
    for result in pool2.findAll('tr', {'valign' : 'baseline'}):#results3:
        #for tmp in result.findAll("tr",{"valign":"baseline"}):
        #print (result)
        
        #print('%s ---------------' % i)
        pagina = Pagina()
        j = 1
        for tmp in result.findAll("td"):

          if j == 1:
            pagina.id = i+1
            pagina.numPag = numPag
            #print 'entrou'
            #print ' %s - %s' %(i, tmp.string)
          #elif j == 2:
            #print ' %s - %s' %(i, tmp.string)
          if j == 3:
            autor = (tmp.string.encode('utf-8')).replace(" \n", "")
            #if autor != ''
            pagina.autor = autor
            #print ' %s - autor %s' %(i, tmp.string)
          if j == 4:
            #print ' %s - %s' %(i, tmp.string)
            titulo = (tmp.string).replace(" \n", "")
            pagina.titulo = titulo.replace("&nbsp","")
          if j == 5:
            #print ' %s - %s' %(i, tmp.string)
            edicao = tmp.string
            #print ' %s - edicao: %s' %(i, edicao)
            if edicao == "None":
              pagina.edicao = ""
            else :
              pagina.edicao = edicao
          if j == 6:
            #print ' %s - %s' %(i, tmp.string)
            ano = (tmp.string)
            #if ano == ""
              #ano = ano
            #else
              #ano.replace(" \n", "")
            pagina.ano = ano
          elif j == 7:
            #print ' %s - %s' %(i, tmp.string)
            l = 1
            bibliot = ''
            for tmp2 in tmp.findAll("a"):
              #biblio = Biblioteca()
              #biblio.id = l
              #biblio.biblio = (tmp2.string).replace("    ","")
              bibliot2 = (tmp2.string).replace("    ","")
              bibliot2 = '<br> '+ bibliot2
              bibliot += bibliot2
              #print ' %s - biblioteca: %s' %(l, tmp2.string)
              #pagina.bibliotecas.append(biblio)

              l += 1
            pagina.bibliotecas = bibliot
          #elif j == 8:
            #print ' %s - %s' %(i, tmp.string)
            #print ' %s - %s' %(j, tmp.string)
          j += 1
        #paginas.append(pagina)
        livro.append(pagina)
        i += 1  

      
    #! imprime o array de dados
    #print obj2json(livros)
    if get_hash:
      try:
        livro.hash = hashlib.sha1(obj2json(livro)).hexdigest()
      except Exception, e:
        print e.message
    return livro

    #print (results3)replace(" \n", "")