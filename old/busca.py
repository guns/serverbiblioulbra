# -*- coding: utf-8 -*-

import sys, json, contextlib, httplib, ssl, urllib2, socket, urllib, cookielib


 # acessa a url da pagina da ulbra para pegar o link de busca
from api.models import Livro
from api.json_utils import obj2json

from BeautifulSoup import BeautifulSoup

base_url = "https://memphis.ulbranet.com.br/ALEPH"

request = urllib2.Request(base_url)
response = urllib2.urlopen(request)

the_page = response.read()
pool = BeautifulSoup(the_page)

results = pool.findAll('form', attrs={'name' : 'form1'})
for result in results:
  linkBusca = result.get('action')
#print(linkBusca)

# realiza a busca no site
url = linkBusca
values = {'func' : 'find-b',
          'request' : 'teste',
          'find_code' : 'WRD',
          'adjacent' : 'N',
          'local_base' : 'ULB01',
          'x' : '40',
          'y' : '8', 
          'filter_code_4' : 'WMA',
          'filter_request_4': '',
          'filter_code_1' : 'WLN',
          'filter_request_1': '',
          'filter_code_2' : 'WYR',
          'filter_request_2': '',
          'filter_code_3' : 'WyR',
          'filter_request_3': ''}

data = urllib.urlencode(values)
req = urllib2.Request(url, data)
response2 = urllib2.urlopen(req)
page = response2.read()
pool2 = BeautifulSoup(page)
#results3 = pool2.findAll('tr', {'valign' : 'baseline'})
#print (results3)
#results3 = pool2.findAll('td', attrs={'class' : 'td1'})
i = 0
livros = []
for result in pool2.findAll('tr', {'valign' : 'baseline'}):#results3:
    #for tmp in result.findAll("tr",{"valign":"baseline"}):
    #print (result)
    
    #print('%s ---------------' % i)
    livro = Livro()
    j = 1
    for tmp in result.findAll("td"):
      if j == 1:
        livro.id = i+1
        #print 'entrou'
        #print ' %s - %s' %(i, tmp.string)
      #elif j == 2:
        #print ' %s - %s' %(i, tmp.string)
      if j == 3:
        autor = (tmp.string.encode('utf-8')).replace(" \n", "")
        #if autor != ''
        livro.autor = autor
        #print ' %s - autor %s' %(i, tmp.string)
      if j == 4:
        #print ' %s - %s' %(i, tmp.string)
        titulo = (tmp.string.encode('utf-8')).replace(" \n", "")
        livro.titulo = titulo
      if j == 5:
        #print ' %s - %s' %(i, tmp.string)
        edicao = tmp.string
        #print ' %s - edicao: %s' %(i, edicao)
        if edicao == "None":
          livro.edicao = ""
        else :
          livro.edicao = edicao
      if j == 6:
        #print ' %s - %s' %(i, tmp.string)
        ano = (tmp.string.encode('utf-8')).replace(" \n", "")
        livro.ano = ano
      #elif j == 7:
        #print ' %s - %s' %(i, tmp.string)
      #elif j == 8:
        #print ' %s - %s' %(i, tmp.string)
        #print ' %s - %s' %(j, tmp.string)
      j += 1
    livros.append(livro)
    i += 1  
#! imprime o array de dados
print obj2json(livros)

#print (results3)